<?php

require 'kernl-update-checker/kernl-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://kernl.us/api/v1/theme-updates/5552a51540e6512d5296fb06/',
	__FILE__,
	'kernl-example-theme',
);
$MyUpdateChecker->collectAnalytics = true;
$MyUpdateChecker->license = "321cba";

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}
add_filter( 'auto_update_theme', '__return_true' );
add_filter( 'auto_update_plugin', '__return_true' );